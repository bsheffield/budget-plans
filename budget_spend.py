import pandas as pd
import plotly.graph_objs as go

# Constants
hourly_rate = 109.77
hours_per_week = 40
weeks_per_year = 52
federal_holidays = 10
pto_hours = 6 * 26  # 6 hours per paycheck, 26 paychecks per year
num_months = 12
work_time_percentage = 0.4

# Calculations
annual_salary = hourly_rate * hours_per_week * weeks_per_year * work_time_percentage
total_work_hours = hours_per_week * weeks_per_year * work_time_percentage
adjusted_work_hours = total_work_hours - (236 * work_time_percentage)
adjusted_annual_salary = hourly_rate * adjusted_work_hours
adjusted_monthly_salary = adjusted_annual_salary / num_months

# Budget allocation for each month
months = pd.date_range(start='2023-10-01', periods=num_months, freq='MS')
budget_allocation = [adjusted_monthly_salary] * num_months

# Cumulative spend calculation
cumulative_spend = [sum(budget_allocation[:i+1]) for i in range(num_months)]

# Creating a DataFrame
budget_spend_plan = pd.DataFrame({
    'Month': months,
    'Budget Allocation': budget_allocation,
    'Cumulative Spend': cumulative_spend
})

# Plotting the spend plan using Plotly
fig = go.Figure()

fig.add_trace(go.Scatter(x=budget_spend_plan['Month'], y=budget_spend_plan['Cumulative Spend'],
                         mode='lines+markers', name='Cumulative Spend'))

fig.update_layout(title='Budget Spend Plan', xaxis_title='Month', yaxis_title='Cumulative Spend',
                  xaxis=dict(tickformat="%b\n%Y"), hovermode='x unified')

fig.show()
