import os
from datetime import datetime

import pandas as pd


def extract_available_balance(sheet_path):
    # Load the RAIDRR tab from the provided spreadsheet path
    raidrr_data = pd.read_excel(sheet_path, sheet_name="RAIDRR")

    # If month is not August (08), insert a blank row after row 15
    if '08-' not in sheet_path:
        raidrr_data = pd.concat([
            raidrr_data.iloc[:15],
            pd.DataFrame([[''] * raidrr_data.shape[1]], columns=raidrr_data.columns),
            raidrr_data.iloc[15:]
        ]).reset_index(drop=True)

    # Extract the "Available Balance" from cells O17 and O18
    w_dms = raidrr_data.iloc[16-1]['Unnamed: 14']
    wout_dms = raidrr_data.iloc[17-1]['Unnamed: 14']

    return w_dms, wout_dms

# Path to the folder containing the spreadsheets
folder_path = 'financials'

# List all files in the folder
all_files = os.listdir(folder_path)

# Filter the list to include only spreadsheets that match "Weekly Report"
all_spreadsheets = [os.path.join(folder_path, file) for file in all_files if "Weekly Report" in file and file.endswith('.xlsx')]

# Dates corresponding to the spreadsheets (Modify these to match your data if needed)
dates = [
    #"06-08-23", "06-14-23", "06-27-23",
    #"07-05-23", "07-12-23", "07-19-23", "07-26-23",
    "08-02-23", "08-09-23", "08-16-23"]
spreadsheets = []
for d in dates:
    for spreadsheet in all_spreadsheets:
        if d in spreadsheet:
            spreadsheets.append(spreadsheet)


# Extracting available balance for each spreadsheet
balances_w_dms = []
balances_wout_dms = []
for spreadsheet in spreadsheets:
    w_dms, wout_dms = extract_available_balance(spreadsheet)
    balances_w_dms.append(w_dms)
    balances_wout_dms.append(wout_dms)

# Creating a DataFrame to display the results
available_balance_data = {
    "Date": dates,
    "Available Balance W/ DMS": balances_w_dms,
    "Available Balance W/OUT DMS": balances_wout_dms
}

available_balance_df = pd.DataFrame(available_balance_data)

print(available_balance_df)

# Convert the dates to datetime format for calculations
available_balance_df["Normalized Date"] = pd.to_datetime(available_balance_df["Date"], format="%m-%d-%y")

# Calculating burn rates for W/O DMS
burn_rates_wo_dms = []
for i in range(1, len(available_balance_df)):
    days_diff = (available_balance_df.iloc[i]["Normalized Date"] - available_balance_df.iloc[i-1]["Normalized Date"]).days
    burn_rate_wo_dms = (available_balance_df.iloc[i - 1]["Available Balance W/OUT DMS"] - available_balance_df.iloc[i]["Available Balance W/OUT DMS"]) / days_diff
    burn_rates_wo_dms.append(burn_rate_wo_dms)

# Averaging out the burn rates
avg_burn_rate_wo_dms = sum(burn_rates_wo_dms) / len(burn_rates_wo_dms)

# Calculate the number of days remaining until the end of FY23
current_date = available_balance_df.iloc[-1]["Normalized Date"]
end_of_fiscal_year = datetime.strptime("2023-09-30", "%Y-%m-%d")
days_remaining = (end_of_fiscal_year - current_date).days

# Projected total expenditure for the remaining days
projected_expenditure_wo_dms = avg_burn_rate_wo_dms * days_remaining

# Forecasting the end-of-year balance
forecasted_balance_wo_dms = available_balance_df.iloc[-1]["Available Balance W/OUT DMS"] - projected_expenditure_wo_dms

print(f"Days remaining is {days_remaining}")
print(f"Total expended in {days_remaining} days is ${days_remaining * avg_burn_rate_wo_dms:.2f}")
print(f"The average daily burn rate without DMS is ${avg_burn_rate_wo_dms:.2f}")
print(f"The projected balance without DMS by September 30, 2023, is ${forecasted_balance_wo_dms:.2f}")
