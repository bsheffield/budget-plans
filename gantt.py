import plotly.figure_factory as ff
import datetime

# Create a list of dictionaries containing tasks, start and end dates, and other information
tasks = [
    {
        'Task': 'Project Kickoff',
        'Start': '2023-10-01',
        'Finish': '2023-10-02',
        'Resource': 'Milestone'
    },
    {
        'Task': 'Requirement Gathering',
        'Start': '2023-10-03',
        'Finish': '2023-11-01',
        'Resource': 'Task'
    },
    {
        'Task': 'Design Phase',
        'Start': '2023-11-02',
        'Finish': '2023-12-15',
        'Resource': 'Task'
    },
    {
        'Task': 'Design Review',
        'Start': '2023-12-16',
        'Finish': '2023-12-17',
        'Resource': 'Deliverable'
    },
    {
        'Task': 'Development Phase',
        'Start': '2023-12-18',
        'Finish': '2024-03-31',
        'Resource': 'Task'
    },
    {
        'Task': 'Testing Phase',
        'Start': '2024-04-01',
        'Finish': '2024-06-30',
        'Resource': 'Task'
    },
    {
        'Task': 'User Acceptance Testing',
        'Start': '2024-07-01',
        'Finish': '2024-07-15',
        'Resource': 'Deliverable'
    },
    {
        'Task': 'Deployment',
        'Start': '2024-07-16',
        'Finish': '2024-07-31',
        'Resource': 'Task'
    },
    {
        'Task': 'Project Closeout',
        'Start': '2024-08-01',
        'Finish': '2024-09-30',
        'Resource': 'Milestone'
    }
]

# Create a Gantt chart using Plotly
fig = ff.create_gantt(tasks, index_col='Resource', title='DoD Fiscal Year 2024 Project Gantt Chart',
                      show_colorbar=True, bar_width=0.4, showgrid_x=True, showgrid_y=True)

fig.show()
